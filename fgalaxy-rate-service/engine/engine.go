package engine

import (
	"gitlab.com/flgalaxy-finsys/finsys/nosql"
	"gitlab.com/flgalaxy-finsys/finsys/fgalaxy-rate-service/config"
	"strings"
	"io"
	"time"
	"strconv"
	"gopkg.in/robfig/cron.v2"
	"github.com/tarantool/go-tarantool"
)

const ETH_ = "ETH/"
const GLP_ = "GLP/"
const GLP_ETH = "GLP/ETH"
const DENOMINATOR = "1000000000000000000";

type Engine struct {
	context *config.AppContext
	currencyRateDAO CurrencyRateDaoInterface
}

type CurrencyRateDaoInterface interface {
	InsertCurrencyRate(currencyRate *nosql.CurrencyRateEntity) (*tarantool.Response, error)
	UpsertCurrencyRate(currencyRate *nosql.CurrencyRateEntity) (*tarantool.Response, error)
	GetCurrentRateByPair(pair string) (*nosql.CurrencyRateEntity, error)
	LastSuccessfulUpdate() (int64, error)
	SetupListOfCurrencies(pairs []string) (*tarantool.Response, error)
	RemoveDataOlderThanTimestamp(timestamp int64) (uint64, error)
	GetRateByPairAndTimestamp(pair string, timestamp int64) (*nosql.CurrencyRateEntity, error)
}

type EngineInterface interface {
	Run() ([]io.Closer)
}

func NewEngine(appContext *config.AppContext, dao CurrencyRateDaoInterface) (Engine) {
	return Engine{appContext, dao}
}

func (engine *Engine) Run() []io.Closer {

	go startEngine(engine)

	return []io.Closer{}
}

func startEngine(engine *Engine) {
	engine.initDatabase()

	engine.setupListOfPairs()

	downtimeHealer := NewDowntimeHealer(engine.context, engine.currencyRateDAO)
	wasDown, err := downtimeHealer.heal()

	if wasDown {
		if err == nil {
			engine.context.Logger.Info("Healing process completed")
		} else {
			engine.context.Logger.Err("Error occurred when healing service after downtime: " + err.Error())
		}
	}

	c := cron.New()
	c.AddFunc(engine.context.Properties.CronForUpdates, engine.updateRates)
	c.AddFunc(engine.context.Properties.CronForCleanupTime, engine.removeOldEntries)
	c.Start()
}

func (engine *Engine) initDatabase() {
	engine.context.Logger.Info("Starting up Database")
	nosql.NewTarantoolClient(engine.context.Properties.NOSQLserver, engine.context.Properties.NOSQLuser, engine.context.Properties.NOSQLpass, engine.context.Properties.NOSQLconn)
}

func (engine *Engine) setupListOfPairs() {
	quotesArray := strings.Split(engine.context.Properties.QuoteCurrencies, ",")
	engine.currencyRateDAO.SetupListOfCurrencies(quotesArray)
}

func (engine *Engine) updateRates() {

	sources := strings.Split(engine.context.Properties.SourcesOrder, ",")

	for _, source := range sources {
		engine.context.Logger.Info("Fetching Rates from " + source)
		rates, err := Sources[source].GetRates(engine.context.Properties.QuoteCurrencies, engine.context.Logger)

		if err != nil {
			engine.context.Logger.Err("Could not fetch data from source: " + source + ", error: " + err.Error())
			continue
		} else {
			fetchedGlpEthRate := true
			glpEthRate, err := fetchCurrentGlpEthRate(engine.context.Logger, engine.currencyRateDAO)
			if err != nil {
				engine.context.Logger.Err(err.Error())
				fetchedGlpEthRate = false
			}

			for _, rate := range rates {
				insert(source, rate, engine.context.Logger, engine.currencyRateDAO)
				if fetchedGlpEthRate {
					insertGlpFiat(source, rate, glpEthRate, engine.context.Logger, engine.currencyRateDAO)
				}
			}

			break
		}
	}
}

func (engine *Engine) removeOldEntries() {
	time := time.Now().AddDate(-1 * engine.context.Properties.RetentionPeriodInYears, 0 ,0)

	resp, err := engine.currencyRateDAO.RemoveDataOlderThanTimestamp(time.Unix())
	if err != nil {
		engine.context.Logger.Err("Something went wrong when cleaning up old data: " + err.Error())
		return
	}

	engine.context.Logger.Info("Cleanup completed, removed: " + strconv.FormatUint(uint64(resp), 10) + " entries")
}