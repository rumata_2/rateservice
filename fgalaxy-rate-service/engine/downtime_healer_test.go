package engine

import (
	"testing"
	"gitlab.com/flgalaxy-finsys/finsys/nosql"
	"github.com/bouk/monkey"
	"reflect"
	"time"
	"github.com/tarantool/go-tarantool"
	"github.com/artjoma/flog"
)

func Test_ShouldIterateOverHoursWhenHealingAndCallAppropriateMethods(t *testing.T) {
	var counter int

	var dao *nosql.CurrencyRateDao

	monkey.PatchInstanceMethod(reflect.TypeOf(dao), "LastSuccessfulUpdate", func(_ *nosql.CurrencyRateDao) (int64, error) {
		return 1532498400, nil
	})
	monkey.Patch(time.Now, func() (time.Time) {
		return  time.Unix(1532509278, 0)
	})
	monkey.Patch(getHistoricData, func(timestamp int64, quoteCurrencies string, logger *flog.Logger) ([]Rate, error) {
		return []Rate{mockRate}, nil
	})
	monkey.PatchInstanceMethod(reflect.TypeOf(dao), "UpsertCurrencyRate", func(_ *nosql.CurrencyRateDao, entity *nosql.CurrencyRateEntity) (_ *tarantool.Response, _ error) {
		return nil, nil
	})
	monkey.PatchInstanceMethod(reflect.TypeOf(dao), "GetRateByPairAndTimestamp", func(_ *nosql.CurrencyRateDao, pair string, timestamp int64) (*nosql.CurrencyRateEntity, error) {
		counter++
		return mockEntity, nil
	})
	defer monkey.UnpatchAll()

	healer := NewDowntimeHealer(createMockedContext("testsource"), dao)

	wasDown, err := healer.heal()

	if !wasDown {
		t.Errorf("Should have been down!")
	}

	if err != nil {
		t.Errorf("Should have been healed!")
	}

	if counter != 3 {
		t.Errorf("Should have 3 hourly iterations, was: %d", counter)
	}
}