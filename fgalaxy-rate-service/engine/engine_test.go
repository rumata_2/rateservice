package engine

import (
	"testing"

	"github.com/artjoma/flog"
	"github.com/pkg/errors"
	"github.com/tarantool/go-tarantool"
	"gitlab.com/flgalaxy-finsys/finsys/fgalaxy-rate-service/config"
	"gitlab.com/flgalaxy-finsys/finsys/nosql"
	"math/big"
	"github.com/bouk/monkey"
	"reflect"
	"time"
)

func Test_ShouldGetRatesFromSource_and_CallInsertOnce(t *testing.T) {

	mockedDao := createMockedDao(123, false, false, mockEntity)
	engine := NewEngine(createMockedContext("testsource"), mockedDao)

	engine.updateRates()

	if !mockedDao.upsertCalled {
		t.Errorf("Upsert was not called")
	}
	if !getRatesCalled {
		t.Error("Get Rates not called!")
	}
}

func Test_WhenFirstSourceFails_ShouldGrabAnotherOne(t *testing.T) {
	mockedDao := createMockedDao(123, false, false, mockEntity)
	engine := NewEngine(createMockedContext("failedsource,testsource"), mockedDao)

	engine.updateRates()

	if !mockedDao.upsertCalled {
		t.Errorf("Upsert was not called")
	}
	if !getRatesCalled {
		t.Error("Get Rates not called!")
	}
	if !getFailedRatesCalled {
		t.Errorf("Method GetRates (failed) not called")
	}
}

func Test_ShouldCreateCorrectArrayOfPairs(t *testing.T) {
	var dao *nosql.CurrencyRateDao
	monkey.PatchInstanceMethod(reflect.TypeOf(dao), "SetupListOfCurrencies", func(_ *nosql.CurrencyRateDao, pairs []string) (_ *tarantool.Response, _ error) {
		if len(pairs) != 2 {
			t.Errorf("Wrong size!")
		}
		if pairs[0] != "AUD" || pairs[1] != "USD" {
			t.Errorf("Wrong elements!")
		}
		return nil, nil
	})
	defer monkey.UnpatchAll()

	mockedDao := createMockedDao(123, false, false, mockEntity)
	engine := NewEngine(createMockedContext("failedsource,testsource"), mockedDao)

	engine.setupListOfPairs()
}

func Test_ShouldFetchGlpEthRate(t *testing.T) {
	var dao *nosql.CurrencyRateDao
	monkey.PatchInstanceMethod(reflect.TypeOf(dao), "GetCurrentRateByPair", func(_ *nosql.CurrencyRateDao, pair string) (_ *nosql.CurrencyRateEntity, _ error) {
		if pair == "GLP/ETH" {
			return mockEntity, nil
		} else {
			t.Errorf("Wrong pair!")
		}
		return nil, nil
	})
	defer monkey.UnpatchAll()

	mockedDao := createMockedDao(123, false, false, mockEntity)

	glpRate, _ := fetchCurrentGlpEthRate(flog.NewLogManagerConsole().NewLogger("test", 1), mockedDao)

	if glpRate != "1.23450000" {
		t.Errorf("Fetched rate incorrect, expected: %s, actual: %s", "1.2345", glpRate)
	}
}

func Test_ShouldMakeGlpFiatConversionsAndCallInsertMethod(t *testing.T) {
	monkey.Patch(insert, func(vendor string, rate Rate, logger *flog.Logger, currencyRateDAO CurrencyRateDaoInterface) {
		if rate.Rate.Cmp(big.NewFloat(0.03944932435)) == 0 {
			t.Errorf("Wrong rate! Expected: %s, Actual: %s", "0.03944932435", rate.Rate)
		} else if rate.CurrencyPair != "GLP/USD" {
			t.Errorf("Wrong pair! Expected: %s, Actual: %s", "GLP/USD", rate.CurrencyPair)
		}
	})
	defer monkey.UnpatchAll()

	mockedDao := createMockedDao(123, false, false, mockEntity)

	insertGlpFiat("test", mockRate, "91106984632664", flog.NewLogManagerConsole().NewLogger("test", 1), mockedDao)
}

func Test_ShouldMakeCorrectTimestampCalculationAndCallDaoMethod(t *testing.T) {

	monkey.Patch(time.Now, func() (time.Time) {
		return time.Unix(1514768461, 0) //corresponds to Monday, January 1, 2018 1:01:01 AM
	})

	var dao *nosql.CurrencyRateDao
	monkey.PatchInstanceMethod(reflect.TypeOf(dao), "RemoveDataOlderThanTimestamp", func(_ *nosql.CurrencyRateDao, timestamp int64) (_ uint64, _ error) {
		if timestamp != 1451610061 { //corresponds to Friday, January 1, 2016 1:01:01 AM
			t.Errorf("Wrong timestamp! expected: 1451610061, actual: %d", timestamp)
		}
		return 5, nil
	})
	defer monkey.UnpatchAll()
	mockedDao := createMockedDao(123, false, false, mockEntity)
	engine := NewEngine(createMockedContext("failedsource,testsource"), mockedDao)

	engine.removeOldEntries()
}

func Test_ShouldRoundTimeCorrectly(t *testing.T) {

	var dao *nosql.CurrencyRateDao
	monkey.PatchInstanceMethod(reflect.TypeOf(dao), "InsertCurrencyRate", func(_ *nosql.CurrencyRateDao, entity *nosql.CurrencyRateEntity) (_ *tarantool.Response, _ error) {
		if entity.Timestamp != 1514768400 {
			t.Errorf("Wrong timestamp expected: 1514768400, actual: %d", entity.Timestamp)
		}
		return
	})
	monkey.Patch(time.Now, func() (time.Time) {
		return  time.Unix(1514768461, 0)
	})
	defer monkey.UnpatchAll()
	mockedDao := createMockedDao(123, false, false, mockEntity)

	insert("test", mockRate, flog.NewLogManagerConsole().NewLogger("test", 1), mockedDao)
}

const (
	PAIR = "ETH/USD"
	RATE = 433.00
)

var currencyRateDao = nosql.NewCurrencyRateDao()
var appContext config.AppContext

type testsource struct{}
type failedsource struct{}

var getRatesCalled bool
var getFailedRatesCalled bool
var insertCalled bool

var mockRate = Rate{CurrencyPair: PAIR, Rate: big.NewFloat(RATE)}
var mockRates = []Rate{mockRate}

var mockEntity = &nosql.CurrencyRateEntity{
	Rate: big.NewFloat(1.2345),
	CurrencyPair: "GLP/ETH",
}

func (self *testsource) GetRates(quotes string, logger *flog.Logger) ([]Rate, error) {
	getRatesCalled = true
	return mockRates, nil
}

func (self *failedsource) GetRates(quotes string, logger *flog.Logger) ([]Rate, error) {
	getFailedRatesCalled = true
	return nil, errors.New("FAIL")
}

func createMockedContext(order string) (*config.AppContext) {
	getRatesCalled = false
	getFailedRatesCalled = false
	insertCalled = false

	instance := testsource{}
	Sources["testsource"] = &instance

	failedInstance := failedsource{}
	Sources["failedsource"] = &failedInstance

	vars := config.Properties{}
	vars.QuoteCurrencies = "AUD,USD"
	vars.SourcesOrder = order
	vars.CronForCleanupTime = "0 * * * *"
	vars.CronForUpdates = "0 0 * * *"
	vars.RetentionPeriodInYears = 2

	return &config.AppContext{nil, flog.NewLogManagerConsole().NewLogger("test", 1), &vars}
}

func createMockedDao(lastSuccessfulTimestamp int64, insertCalled bool, upsertCalled bool, entity *nosql.CurrencyRateEntity) *CurrencyRateMockedDao {
	return &CurrencyRateMockedDao{lastSuccessfulTimestamp, insertCalled, upsertCalled, entity}
}

type CurrencyRateMockedDao struct {
	lastSuccessfulTimestamp int64
	insertCalled bool
	upsertCalled bool
	entity *nosql.CurrencyRateEntity
}

func (currencyRateMockDao *CurrencyRateMockedDao) LastSuccessfulUpdate() (int64, error) {
	return currencyRateMockDao.lastSuccessfulTimestamp, nil
}

func (currencyRateMockDao *CurrencyRateMockedDao) InsertCurrencyRate(currencyRate *nosql.CurrencyRateEntity) (*tarantool.Response, error) {
	currencyRateMockDao.insertCalled = true
	return nil, nil
}

func (currencyRateMockDao *CurrencyRateMockedDao) UpsertCurrencyRate(currencyRate *nosql.CurrencyRateEntity) (*tarantool.Response, error) {
	currencyRateMockDao.upsertCalled = true
	return nil, nil
}

func (currencyRateMockDao *CurrencyRateMockedDao) GetCurrentRateByPair(pair string) (*nosql.CurrencyRateEntity, error) {
	return currencyRateMockDao.entity, nil
}

func (currencyRateMockDao *CurrencyRateMockedDao) SetupListOfCurrencies(pairs []string) (*tarantool.Response, error) {
	return nil, nil
}

func (currencyRateMockDao *CurrencyRateMockedDao) RemoveDataOlderThanTimestamp(timestamp int64) (uint64, error) {
	return 123, nil
}

func (currencyRateMockDao *CurrencyRateMockedDao) GetRateByPairAndTimestamp(pair string, timestamp int64) (*nosql.CurrencyRateEntity, error) {
	return nil, nil
}