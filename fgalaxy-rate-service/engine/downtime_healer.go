package engine

import (
	"time"
	"fmt"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"math/big"
	"strconv"
	"gitlab.com/flgalaxy-finsys/finsys/fgalaxy-rate-service/config"
	"strings"
	"github.com/pkg/errors"
	"github.com/artjoma/flog"
)

const (
	CRYPTOCOMPARE_HISTORIC        = "https://min-api.cryptocompare.com/data/pricehistorical?fsym=ETH&tsyms=%s&ts=%s"
	CRYPTOCOMPARE_HISTORIC_SOURCE = "cryptocompare_historic"
	BEGINNING_OF_TIME             = 1526428800
)

type Healer interface {
	heal() (wasDown bool, isHealed bool, err error)
}

type DowntimeHealer struct {
	context         *config.AppContext
	currencyRateDAO CurrencyRateDaoInterface
}

func NewDowntimeHealer(appContext *config.AppContext, currencyRateDAO CurrencyRateDaoInterface) DowntimeHealer {
	return DowntimeHealer{appContext, currencyRateDAO}
}

func (downtimeHealer *DowntimeHealer) heal() (wasDown bool, err error) {

	lastUpdateTimestamp, err := downtimeHealer.currencyRateDAO.LastSuccessfulUpdate()
	if err != nil {
		downtimeHealer.context.Logger.Err("Something went wrong when getting last successful update : " + err.Error())
	}
	var nextPotentialUpdate int64
	if lastUpdateTimestamp == 0 {
		nextPotentialUpdate = BEGINNING_OF_TIME
	} else {
		nextPotentialUpdate = lastUpdateTimestamp + 3600
	}

	for nextPotentialUpdate < time.Now().Unix() {

		wasDown = true;
		rates, err := getHistoricData(nextPotentialUpdate, downtimeHealer.context.Properties.QuoteCurrencies, downtimeHealer.context.Logger)

		if err != nil {
			downtimeHealer.context.Logger.Err("Something went wrong when getting historic data")
			return true, err
		}

		glpEthRate, err := downtimeHealer.currencyRateDAO.GetRateByPairAndTimestamp(GLP_ETH, nextPotentialUpdate)
		if err != nil {
			return true, errors.New("Could not fetch GLP/ETH rate for timestamp: " + string(nextPotentialUpdate) + ", reason: " + err.Error())
		}

		for _, rate := range rates {
			rate.Timestamp = nextPotentialUpdate
			insert(CRYPTOCOMPARE_HISTORIC_SOURCE, rate, downtimeHealer.context.Logger, downtimeHealer.currencyRateDAO)
			insertGlpFiat(CRYPTOCOMPARE_HISTORIC_SOURCE, rate, glpEthRate.Rate.Text('f', 8), downtimeHealer.context.Logger, downtimeHealer.currencyRateDAO)
		}

		downtimeHealer.context.Logger.Info("Populated historic data, timestamp: " + strconv.FormatInt(nextPotentialUpdate, 10) + ", date: " + time.Unix(nextPotentialUpdate, 0).Format(time.RFC822))

		nextPotentialUpdate = nextPotentialUpdate + 3600
		time.Sleep(2 * time.Second) //sleep not to exceed the limit of calls
	}

	return wasDown, nil
}

func getHistoricData(timestamp int64, quoteCurrencies string, logger *flog.Logger) ([]Rate, error) {

	//there is limitation of the characters used for the quotes = 30
	//need 4 calls to iterate over 32 currencies
	splitedQuotes := strings.Split(quoteCurrencies, ",")

	tempQuotes := ""
	rates := make([]Rate, 0)
	for i := 0; i < len(splitedQuotes); i++ {
		tempQuotes = tempQuotes + splitedQuotes[i] + ","

		if len(tempQuotes) > 26 || i+1 >= len(splitedQuotes) {
			call := fmt.Sprintf(CRYPTOCOMPARE_HISTORIC, tempQuotes, strconv.FormatInt(timestamp, 10))
			tempQuotes = ""
			callSuccessful := false
			for {
				if !callSuccessful {
					response, err := http.Get(call)
					if err != nil {
						return nil, err
					} else {
						defer response.Body.Close()
						contents, err := ioutil.ReadAll(response.Body)
						if err != nil {
							return nil, err
						}

						responseMap := map[string]map[string]float64{}
						err = json.Unmarshal(contents, &responseMap)
						if err != nil {
							logger.Err("Something went wrong: " + err.Error())
							logger.Err("Response contents: " + string(contents))
							logger.Err("Retry in 10 seconds")
							time.Sleep(10 * time.Second)
							continue
						}
						for currency, currencyRate := range responseMap["ETH"] {
							newRate := NewRateWithATimestamp(CreatePair("ETH", currency), big.NewFloat(currencyRate), timestamp)
							rates = append(rates, *newRate)
						}

						callSuccessful = true
					}
				} else {
					break
				}
			}
		}
	}
	return rates, nil
}
