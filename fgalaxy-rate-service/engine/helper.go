package engine

import (
	"strings"
	"math/big"
	"time"
	"gitlab.com/flgalaxy-finsys/finsys/nosql"
	"github.com/artjoma/flog"
	"github.com/pkg/errors"
	"strconv"
)

func insertGlpFiat(vendor string, ethFiatRate Rate, glpEthRateInString string, logger *flog.Logger, currencyRateDAO CurrencyRateDaoInterface) {
	currencyPair := GLP_ + strings.Split(ethFiatRate.CurrencyPair, "/")[1]

	fiatRate := ethFiatRate.Rate

	glpEthRate, noErrors := new(big.Float).SetString(glpEthRateInString)
	if !noErrors {
		logger.Err("Something went wrong went converting glpRate")
		return
	}

	denominator, noErrors := new(big.Float).SetString(DENOMINATOR)
	if !noErrors {
		logger.Err("Something went wrong went creating denominator")
		return
	}

	//changing the rate from WEI to ETH
	glpEthRate.Quo(glpEthRate, denominator)

	//converting rates
	glpFiatRate := big.NewFloat(0).Mul(fiatRate, glpEthRate)

	glpFiatRateObject := Rate{
		CurrencyPair: currencyPair,
		Rate:         glpFiatRate,
		Timestamp:    ethFiatRate.Timestamp,
	}

	insert(vendor, glpFiatRateObject, logger, currencyRateDAO)
}

type Rate struct {
	CurrencyPair string
	Rate         *big.Float
	Timestamp	 int64
}

func NewRate(currencyPair string, rate *big.Float) *Rate {
	return &Rate{CurrencyPair: currencyPair, Rate: rate}
}

func NewRateWithATimestamp(currencyPair string, rate *big.Float, timestamp int64) *Rate {
	return &Rate{CurrencyPair: currencyPair, Rate: rate, Timestamp: timestamp}
}

type Source interface {
	GetRates(quotes string, logger *flog.Logger) ([]Rate, error)
}

func CreatePair(base string, quote string) string {
	return base + "/" + quote
}

var Sources map[string]Source

func init() {
	Sources = make(map[string]Source)
}

func insert(vendor string, rate Rate, logger *flog.Logger, currencyRateDAO CurrencyRateDaoInterface) {
	timeNow := time.Now()
	if rate.Timestamp == 0 {
		rate.Timestamp = time.Date(timeNow.Year(), timeNow.Month(), timeNow.Day(), timeNow.Hour(), 0, 0, 0, timeNow.Location()).Unix()
	}

	entity := nosql.CurrencyRateEntity{
		CurrencyPair:  rate.CurrencyPair,
		Rate:          rate.Rate,
		VendorId:      vendor,
		Timestamp: 	   uint32(rate.Timestamp),
		TimestampReal: uint32(timeNow.Unix()),
	}

	_, err := currencyRateDAO.UpsertCurrencyRate(&entity)

	if err != nil {
		logger.Err("Error inserting data into table: " + err.Error())
		logger.Err("Entity details: " + rate.CurrencyPair + " " + strconv.FormatInt(rate.Timestamp, 10))
		return
	}
}

func fetchCurrentGlpEthRate(logger *flog.Logger, currencyRateDAO CurrencyRateDaoInterface) (string, error) {
	entity, err := currencyRateDAO.GetCurrentRateByPair(GLP_ETH)
	if err != nil {
		logger.Err("Could not fetch current GLP rate")
		return "", errors.New("Could not fetch current GLP rate: " + err.Error())
	}
	return entity.Rate.Text('f',8), nil
}