package config

import (
	"os"
	"fmt"
	"path/filepath"
	"encoding/json"
	"github.com/artjoma/flog"
)

const (
	APP_CONFIG_FILE_NAME = "config.json"
	APP_MAIN_LOGGER      = "main"

)

type Properties struct {
	NOSQLserver  string `json:"NOSQLserver"`
	NOSQLuser    string `json:"NOSQLuser"`
	NOSQLpass    string `json:"NOSQLpass"`
	NOSQLconn    uint32 `json:"NOSQLconn"`

	SourcesOrder string `json:"SourcesOrder"`

	QuoteCurrencies string `json:"QuoteCurrencies"`

	CronForUpdates string `json:"CronForUpdates"`
	CronForCleanupTime string `json:"CronForCleanupTime"`

	RetentionPeriodInYears int `json:"RetentionPeriodInYears"`

	LogFileSize uint64 `json:"LogFileSize"`
	LoggerConsole bool `json:"LoggerConsole"`
	LogThreshold rune  `json:"LogThreshold"`
}

type AppContext struct {
	LogManager *flog.LogManager
	Logger     *flog.Logger
	Properties 	   *Properties
}

func GetContext() *AppContext {
	workPath, err := os.Getwd()
	if err != nil {
		panic("can't get work path")
	}

	fmt.Println("work path: ", workPath)
	configFile, err := os.Open(filepath.Join(workPath, APP_CONFIG_FILE_NAME))
	if err != nil {
		panic("application config file doesn't exist !")
	}
	defer configFile.Close()
	properties := &Properties{}
	err = json.NewDecoder(configFile).Decode(properties)
	if err != nil {
		panic("invalid config file! " + err.Error())
	}

	fmt.Println("logger console: ", properties.LoggerConsole)
	var logManager *flog.LogManager = nil
	if properties.LoggerConsole {
		logManager = flog.NewLogManagerConsole()
	} else {
		logManager = flog.NewLogManagerFile(workPath, properties.LogFileSize)
	}
	fmt.Printf("logThreshold: %q\n", properties.LogThreshold)
	//create main logger
	logger := logManager.NewLogger(APP_MAIN_LOGGER, properties.LogThreshold)

	return &AppContext{logManager, logger, properties}
}