#!/usr/bin/env bash
go test ./...
go version
go clean
go build -a -ldflags="-s -w"
mv fgalaxy-rate-service build/rateservice