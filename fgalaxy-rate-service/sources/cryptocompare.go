package sources

import (
	"gitlab.com/flgalaxy-finsys/finsys/fgalaxy-rate-service/engine"
	"fmt"
	"encoding/json"
	"net/http"
	"io/ioutil"
	"github.com/artjoma/flog"
	"math/big"
)

const (
	CRYPTOCOMPARE = "https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=%s"
)

type cryptocompare struct {}

func (self *cryptocompare) GetRates(quotes string, logger *flog.Logger) ([]engine.Rate, error) {
	call := fmt.Sprintf(CRYPTOCOMPARE, quotes)

	response, err := http.Get(call)
	if err != nil {
		return nil, err
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return nil, err
		}

		responseMap := map[string]interface{}{}
		err = json.Unmarshal(contents, &responseMap)
		if err != nil {
			return nil, err
		}

		rates := make([]engine.Rate, 0)
		for currency, currencyRate := range responseMap {
			newRate := engine.NewRate(engine.CreatePair("ETH", currency), big.NewFloat(currencyRate.(float64)))
			rates = append(rates, *newRate)
		}

		return rates, nil
	}
}

func init() {
	instance := cryptocompare{}
	engine.Sources["cryptocompare"] = &instance
}