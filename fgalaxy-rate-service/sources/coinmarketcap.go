package sources

import (
	"gitlab.com/flgalaxy-finsys/finsys/fgalaxy-rate-service/engine"
	"fmt"
	"net/http"
	"strings"
	"io/ioutil"
	"encoding/json"
	"github.com/artjoma/flog"
	"math/big"
	"errors"
)

const (
	COINMARKETCAP = "https://api.coinmarketcap.com/v1/ticker/ethereum/?convert=%s"
)

type coinmarketcap struct{}

func (self *coinmarketcap) GetRates(quotes string, logger *flog.Logger) ([]engine.Rate, error) {
	quotesArray := strings.Split(quotes, ",")
	rates := make([]engine.Rate, 0)
	for _, quote := range quotesArray {
		call := fmt.Sprintf(COINMARKETCAP, quote)
		response, err := http.Get(call)

		if err != nil {
			return nil, err
		} else {
			defer response.Body.Close()
			contents, err := ioutil.ReadAll(response.Body)
			if err != nil {
				return nil, err
			}

			responseMap := []map[string]interface{}{}
			err = json.Unmarshal(contents, &responseMap)
			if err != nil {
				return nil, err
			}
			for key, value := range responseMap[0] {
				if key == "price_" +  strings.ToLower(quote) {
					rateInFloat, ok := new(big.Float).SetString(value.(string))
					if !ok {
						return nil, errors.New("Could not parse rate: " + value.(string))
					}
					newRate := engine.NewRate(engine.CreatePair("ETH", quote), rateInFloat)
					rates = append(rates, *newRate)
				}
			}
		}
	}
	return rates, nil
}

func init() {
	instance := coinmarketcap{}
	engine.Sources["coinmarketcap"] = &instance
}