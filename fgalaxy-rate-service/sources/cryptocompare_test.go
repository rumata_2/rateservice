package sources

import (
	"io"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/bouk/monkey"
	"math/big"
)

var path_cryptocompare string = "https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD"

func Test_GetRates_cryptocompare(t *testing.T) {

	monkey.Patch(http.Get, func(path string) (*http.Response, error) {
		if path == path_cryptocompare {
			return &mockedResponse, nil
		} else {
			t.Errorf("Incorrect path!")
			return nil, nil
		}
	})

	monkey.Patch(ioutil.ReadAll, func(r io.Reader) ([]byte, error) {
		return []byte(mockedMessage_cryptocompare), nil
	})

	defer monkey.UnpatchAll()

	source := cryptocompare{}
	rates, err := source.GetRates(QUOTES, nil)

	if err != nil {
		t.Errorf(err.Error())
	}

	if len(rates) != 1 {
		t.Errorf("Wrong length!")
	}

	if rates[0].CurrencyPair != "ETH/USD" || rates[0].Rate.Cmp(big.NewFloat(123.432)) != 0 {
		t.Errorf("Wrong response! %d %s", rates[0].Rate, "123.432")
	}
}

var mockedMessage_cryptocompare = "{\"USD\":123.432}"
