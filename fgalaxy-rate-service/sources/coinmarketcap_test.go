package sources

import (
	"io"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/bouk/monkey"
	"math/big"
)

const (
	QUOTES = "USD"
)

var path_coinmarketcap string = "https://api.coinmarketcap.com/v1/ticker/ethereum/?convert=USD"

type ReadCloserMock struct{}

func (self *ReadCloserMock) Read(p []byte) (n int, err error) {
	return 0, nil
}

func (self *ReadCloserMock) Close() error {
	return nil
}

var mockedResponse http.Response = http.Response{Body: &ReadCloserMock{}}

func Test_GetRates_coinmarketcap(t *testing.T) {

	monkey.Patch(http.Get, func(path string) (*http.Response, error) {
		if path == path_coinmarketcap {
			return &mockedResponse, nil
		} else {
			t.Errorf("Incorrect path!")
			return nil, nil
		}
	})

	monkey.Patch(ioutil.ReadAll, func(r io.Reader) ([]byte, error) {
		return []byte(mockedMessage_coinmarketcap), nil
	})

	defer monkey.UnpatchAll()

	source := coinmarketcap{}
	rates, err := source.GetRates(QUOTES, nil)

	if err != nil {
		t.Errorf(err.Error())
	}

	if len(rates) != 1 {
		t.Errorf("Wrong length!")
	}

	if rates[0].CurrencyPair != "ETH/USD" || rates[0].Rate.String() != big.NewFloat(9329.66).String() {
		t.Errorf("Wrong response! %d %s", rates[0].Rate, "9329.66")
	}
}

var mockedMessage_coinmarketcap = "[" +
	"    {" +
	"        \"id\": \"bitcoin\", " +
	"        \"name\": \"Bitcoin\", " +
	"        \"symbol\": \"BTC\", " +
	"        \"rank\": \"1\", " +
	"        \"price_usd\": \"9329.66\", " +
	"        \"price_btc\": \"1.0\", " +
	"        \"24h_volume_usd\": \"8168830000.0\", " +
	"        \"market_cap_usd\": \"158724218087\", " +
	"        \"available_supply\": \"17012862.0\", " +
	"        \"total_supply\": \"17012862.0\", " +
	"        \"max_supply\": \"21000000.0\", " +
	"        \"percent_change_1h\": \"1.25\", " +
	"        \"percent_change_24h\": \"2.17\", " +
	"        \"percent_change_7d\": \"5.41\", " +
	"        \"last_updated\": \"1525353272\", " +
	"        \"price_eur\": \"7786.42228008\", " +
	"        \"24h_volume_eur\": \"6817607492.04\", " +
	"        \"market_cap_eur\": \"132469327725\"" +
	"    }" +
	"]"
