package main

import (
	"gitlab.com/flgalaxy-finsys/finsys/fgalaxy-rate-service/engine"
	_ "gitlab.com/flgalaxy-finsys/finsys/fgalaxy-rate-service/config"
	_ "gitlab.com/flgalaxy-finsys/finsys/fgalaxy-rate-service/sources"
	"gitlab.com/flgalaxy-finsys/finsys/fgalaxy-rate-service/config"
	"os"
	"os/signal"
	"syscall"
	"io"
	"time"
	"gitlab.com/flgalaxy-finsys/finsys/nosql"
)

func main() {
	context := config.GetContext()

	logger := context.Logger
	logger.Info("Starting up Rate Service")
	logger.Info("Quote Currencies: " + context.Properties.QuoteCurrencies)

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGKILL)

	engine := engine.NewEngine(context, nosql.NewCurrencyRateDao())
	closable := engine.Run()

	sig := <-sigs
	logger.Info("Shutdown. Signal: " + sig.String())
	close(sigs)

	destroy(*context, closable)
}

func destroy(context config.AppContext, closers []io.Closer) {
	context.Logger.Info("Destroy application")

	context.Logger.Info("Shutting down modules")

	for _, closer := range closers {
		closer.Close()
	}

	context.Logger.Info("Destroyed gracefully")

	time.Sleep(1 * time.Second)
	context.LogManager.DestroyLogManager()
}
