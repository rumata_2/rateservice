# Rate Service

## Basic Info
This service is fetching currency rates (ETH/FIAT) from external services and storing them in Tarantool DB.
There are two sources for the currency pairs. When one fails the other takes over.

When ETH/FIAT pairs are fetched, current GLP rate (GLP is our internal blockchain currency) is taken from our DB and GLP/FIAT pairs are calculated.

There is a process that runs every night that removes entries older than 5 years.

There is a downtime healer which adds historic entries if missing.

The addition of the GLP rate was not handled by RateService but rather by other service called Lightapi - api that was handling our blockchain.

## HTTP calls
RateService was not exposing any api, instead we used our database itself (see: nosql\schema\api\router.lua)

## Granularity
RateService was adding data in a way which then allowed us to retrieve data with set granularity, for example: give me all hourly rates between x and y.
Unfortunately logic that supports that was added on the database side - not sure if you will be able to reuse that (see: nosql\schema\db_currencyrate.lua).

## Config
In the config.json there are variables defining number of FIAT currencies and frequency of requests.
One can also specify order of sources and scheduling with cron.