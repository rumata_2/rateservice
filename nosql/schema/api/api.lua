--
-- utils for API
--
local MAX_TX_COUNT_PER_REQUEST 		= 255
local MAX_TX_TIME_RANGE_PER_REQUEST = 5356800 --- 62 days in UNIX time

function validateAddress(address)
	if string.len(address) == 42 then
		return nil
	end

	return {
		err = "INVALID_REQUEST";
		detail = "invalid hash format";
		code = 1;
	}
end

function validateTx(txHash)
	if string.len(txHash) == 66 then
		return nil
	end

	return {
		err = "INVALID_REQUEST";
		detail = "invalid hash format";
		code = 1;
	}
end

-- truncate count
function checkMaxTxCountPerRequest(count)
	if count > MAX_TX_COUNT_PER_REQUEST then
		return MAX_TX_COUNT_PER_REQUEST
	end

	return count
end

-- truncate to date
function checkMaxTxRangePerRequest(from, to)
	if to - from > MAX_TX_TIME_RANGE_PER_REQUEST then
		return from + MAX_TX_TIME_RANGE_PER_REQUEST
	end

	return to
end

function TxArrayToJson(entityArr)
	local list = {}

	for i, record in ipairs(entityArr) do
		list[i] = TxToJson(record)
	end

	return list
end

function TxToJson(record)
	return {
		txId = record[2];
		timestamp = record[3];
		platformId = record[4];
		type = record[5];
		fromAddress = record[6];
		toAddress = record[7];
		amount = record[8];
		glpRate = record[9];
		orderId = record[10];
		nType = record[11];
		currency = record[12];
	}
end

function ExtTxArrayToJson(entityArr)
	local list = {}

	for i, record in ipairs(entityArr) do
		list[i] = ExtTxToJson(record)
	end

	return list
end

function ExtTxToJson(record)
	return {
	    type = record[2];
		status = record[12];
		glpRate = record[8];
		glpAmount = record[7];
		amount = record[6];
		timestamp = record[11];
		txId = record[10];
		address = record[5];
		intTxId = record[9];
		methodType = record[13];
	}
end


function CurrencyRatesToJson(ratesArray)
	local response = {}

	for i, record in ipairs(ratesArray) do
		response[i] = CurrencyRateToJson(record)
	end

	return response
end

function CurrencyRateToJson(rate)
	if rate == nil then
		return {}
	end

	local response = {}
	response[1] = rate[2]
	response[2] = rate[3]

	return response
end

function variousPairsRatesToJson(rates)
	local response = {}
	for i, record in ipairs(rates) do
		response[i] = {
			record[1];
			record[2];
			record[3];
		}
	end

	return response
end

function generateResponseWithError(req, err, code, details, status)
	local model = {
		err = err;
		code = code;
		details = details;
	}
	local response = req:render({ json = model })
	response.status = status

	return response
end


