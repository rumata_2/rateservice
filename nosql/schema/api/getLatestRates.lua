require("api.api")

-- Example call: /api2/rate/ETH/USD

local exports = {}

exports.getLatestRates = function (req)
    local rates = getLatestRates()

    local model = {
        result = variousPairsRatesToJson(rates);
    }

    return req:render({ json = model })
end

return exports
