require("api.api")

-- Example call: /api2/rate/ETH/USD/21321312312

local exports = {}

exports.currRateByPairAndTimestamp = function (req)
    local base = req:stash('base')
    local quote = req:stash('quote')

    local timestamp = tonumber(req:stash('timestamp'), 10)
    if timestamp == nil then
        return generateResponseWithError(req, "INVALID_REQUEST", 1, 'Wrong parameters', 404)
    end

    if base == quote then
        return generateResponseWithError(req, "INVALID_REQUEST", 1, 'Base currency equal to quote currency', 404)
    end

    local rate = currencyRateByPairAndTimestamp(base .. "/" .. quote, timestamp)
    local model = {
        result = {
            pair = base .. "/" .. quote;
            rates = CurrencyRateToJson(rate);
        }
    }

    if rate == nil then
        return generateResponseWithError(req, "INVALID_REQUEST", 1, 'No rate available for this pair/date', 404)
    end

    if rate == "TIMESTAMP_TOO_OLD" then
        return generateResponseWithError(req, "INVALID_REQUEST", 1, 'Timestamp too old', 404)
    end

    return req:render({ json = model })
end

return exports
