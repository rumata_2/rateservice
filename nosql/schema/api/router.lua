-- Finsys Http storage API
--
log = require('log')

local exports = {}
local httpd = {}

-- init http server socket
exports.initHttpAPI = function()
	log.info("start init http API!")

	httpd = require('http.server').new(
		nil,
		8090,
		{
			max_header_size = 768,
			header_timeout = 5,
			charset = 'UTF-8',
			log_requests = false
		}
	)
	--
	-- controllers, see exports.stopHttpAPI how to free resources
	--
	local handler

	handler = require("api.addressStatic")
	httpd:route({ path = '/api2/wallet/:address/static'}, handler.addressStatic)

	handler = require("api.getWExtTxN")
	httpd:route({ path = '/api2/wallet/:address/exttx/n/:count'}, handler.getWExtTxN)

	handler = require("api.getWExtTxByRange")
	httpd:route({ path = '/api2/wallet/:address/exttx/time/:from/:to'}, handler.getWExtTxByRange)

	handler = require("api.getWTxN")
	httpd:route({ path = '/api2/wallet/:address/tx/n/:count'}, handler.getWTxN)

	handler = require("api.getWTxByRange")
	httpd:route({ path = '/api2/wallet/:address/tx/time/:from/:to'}, handler.getWTxByRange)

	handler = require("api.getWTxAllByRange")
	httpd:route({ path = '/api2/wallet/:address/tx-merged/:from/:to'}, handler.getWTxAllByRange)

	handler = require("api.getCurrencyList")
	httpd:route({ path = '/api2/fin/rate/list'}, handler.currencyList)

	handler = require("api.getCurrentRate")
	httpd:route({ path = '/api2/fin/rate/:base/:quote'}, handler.currentCurrencyRate)

	handler = require("api.getCurrencyRatesByPairAndTimeRange")
	httpd:route({ path = '/api2/fin/rate/:base/:quote/:from/:to'}, handler.currRatesByPairAndTimeRange)

	handler = require("api.getCurrencyRatesByPairAndTimerangeAndGranularity")
	httpd:route({ path = '/api2/fin/rate/:base/:quote/:from/:to/:granularity'}, handler.currRatesByPairAndTimeRangeAndGranularity)

	handler = require("api.getCurrencyRateByPairAndTimestamp")
	httpd:route({ path = '/api2/fin/rate/:base/:quote/:timestamp'}, handler.currRateByPairAndTimestamp)

	handler = require("api.getLatestRates")
	httpd:route({ path = '/api2/fin/rate/latest/glp/fiat'}, handler.getLatestRates)

	-- get internal tx by hash
	handler = require("api.getTxByHash")
	httpd:route({ path = '/api2/storage/tx/:txHash'}, handler.getTxByHash)

	httpd:start()
	log.info("end init http API")
end

-- stop http server socket and free resources
exports.stopHttpAPI = function()
	httpd:stop()
	package.loaded['api.api'] = nil
	package.loaded['api.addressStatic'] = nil
	package.loaded['api.getWTxN'] = nil
	package.loaded['api.getWTxByRange'] = nil
	package.loaded['api.getWTxAllByRange'] = nil
	package.loaded['api.getWExtTxByRange'] = nil
	package.loaded['api.currencyPairs'] = nil
	package.loaded['api.currRatesByPairAndTimeRange'] = nil
	package.loaded['api.currRateByPairAndTimestamp'] = nil
	package.loaded['api.getCurrentRate'] = nil
	package.loaded['api.getLatestRates'] = nil
	package.loaded['api.getTxByHash'] = nil

	log.info("end stop http API!")
end


return exports
