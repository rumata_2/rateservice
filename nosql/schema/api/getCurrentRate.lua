require("api.api")

-- Example call: /api2/rate/ETH/USD

local exports = {}

exports.currentCurrencyRate = function (req)
    local base = req:stash('base')
    local quote = req:stash('quote')

    if base == quote then
        return generateResponseWithError(req, "INVALID_REQUEST", 1, 'Base currency equal to quote currency', 404)
    end

    local rate = currentCurrencyRateByPair(base .. "/" .. quote)
    local model = {
        result = {
            pair = base .. "/" .. quote;
            rates = CurrencyRateToJson(rate);
        }
    }

    if rate == nil then
        return generateResponseWithError(req, "INVALID_REQUEST", 1, 'No rate available for this pair', 404)
    end

    return req:render({ json = model })
end

return exports
