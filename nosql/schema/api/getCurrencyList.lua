require("api.api")

-- Example call: /api2/rate/currencypairs

local exports = {}

exports.currencyList = function (req)
	local model = {
		result = getFiatCurrencyList()
	}

	return req:render({json = model})
end

return exports
