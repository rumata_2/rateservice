
require("api.api")

-- Example call: /api2/rate/ETH/USD/4432423234/432243423

local exports = {}

exports.currRatesByPairAndTimeRangeAndGranularity = function (req)
    local base = req:stash('base')
    local quote = req:stash('quote')
    local granularity = req:stash('granularity')

    local fromNum = tonumber(req:stash('from'), 10)
    if fromNum == nil then
        return generateResponseWithError(req, "INVALID_REQUEST", 1, 'Wrong parameters', 404)
    end

    local toNum = tonumber(req:stash('to'), 10)
    if toNum == nil then
        return generateResponseWithError(req, "INVALID_REQUEST", 1, 'Wrong parameters', 404)
    end

    if fromNum > toNum then
        return generateResponseWithError(req, "INVALID_REQUEST", 1, 'From_timestamp greater than to_timestamp', 404)
    end

    if base == quote then
        return generateResponseWithError(req, "INVALID_REQUEST", 1, 'Base currency equal to quote currency', 404)
    end

    local rates = getTicks(base .. "/" .. quote, fromNum, toNum, granularity)

    if rates == "EXCEDEED_1000_TICKS" then
        return generateResponseWithError(req, "INVALID_REQUEST", 1, 'Exceeded 1000 ticks, please amend your query', 404)
    end

    if rates == "GRANULARITY_NOT_SUPPORTED" then
        return generateResponseWithError(req, "INVALID_REQUEST", 1, 'Granularity for that pair not supported, please amend your query', 404)
    end

    if rates == "TIMESTAMP_TOO_OLD" then
        return generateResponseWithError(req, "INVALID_REQUEST", 1, 'Timestamp too old', 404)
    end

    if #rates == 0 then
        return generateResponseWithError(req, "INVALID_REQUEST", 1, 'No rates available for this pair/date', 404)
    end

    local model = {
        result = {
            pair = base .. "/" .. quote;
            rates = CurrencyRatesToJson(rates);
        }
    }

    return req:render({json = model})
end

return exports
