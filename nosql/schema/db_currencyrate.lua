---
--- currencypair_rates
---
if not box.space.currencypair_rate then
    log.info('start create currencypair_rate space')
    currencyRate = box.schema.space.create("currencypair_rate", {
        id = 20,
        engine = 'vinyl',
        format = {
            { name = 'CURRENCY_PAIR', type = 'string' },
            { name = 'RATE', type = 'string' },
            { name = 'TIMESTAMP', type = 'unsigned' },
            { name = 'TIMESTAMP_REAL', type = 'unsigned' },
            { name = 'VENDOR_ID', type = 'string' }
        },
    })

    currencyRate:create_index('currencyPairTimestampIndex', {
        type = 'tree',
        unique = true,
        parts = { 1, 'string', 3, 'unsigned' },
    })

    currencyRate:create_index('currencyPairIndex', {
        type = 'tree',
        unique = false,
        parts = { 1, 'string' },
    })

    currencyRate:create_index('rateIndex', {
        type = 'tree',
        unique = false,
        parts = { 2, 'string' },
    })

    currencyRate:create_index('timestampIndex', {
        type = 'tree',
        unique = false,
        parts = { 3, 'unsigned' },
    })

    currencyRate:create_index('timestampRealIndex', {
        type = 'tree',
        unique = false,
        parts = { 4, 'unsigned' },
    })

    log.info('end create')
end

granularityMap = {}
granularityMap["4s"] = 4
granularityMap["h"] = 3600
granularityMap["4h"] = 14400
granularityMap["6h"] = 21600
granularityMap["12h"] = 43200
granularityMap["d"] = 86400
granularityMap["w"] = 604800
granularityMap["m"] = 2592000


function getTicks(pair, from, to, sampleType)

    if (sampleType == "4s" and pair ~= "GLP/ETH") then
        return "GRANULARITY_NOT_SUPPORTED"
    end

    local response = {}
    local granularity = granularityMap[sampleType]

    local beginningOfTime = 1526428800
    if (from < beginningOfTime or to < beginningOfTime) then
        return "TIMESTAMP_TOO_OLD"
    end

    local timeNow = os.time(os.date("!*t"))
    if (from > timeNow) then
        from = timeNow
    end
    if (to > timeNow) then
        to = timeNow
    end

    local openingBracket = math.roundUp(from, granularity)
    local closingBracket = math.roundDown(to, granularity)

    local iterator = openingBracket

    local i = 1
    while (iterator <= closingBracket) do
        local tick = box.space.currencypair_rate.index.currencyPairTimestampIndex:get({ pair, iterator })
        if tick == nil then
            tick = findAlternativeTick(pair, iterator)
        end
        response[i] = tick
        iterator = iterator + granularity
        i = i + 1

        if (#response > 1000) then
            return "EXCEDEED_1000_TICKS"
        end
    end

    return response
end

function findAlternativeTick(pair, timestamp)
    local tick = nil
    local iterator = timestamp
    local gapsTable = {}
    local i = 0
    local beginningOfTime = 1526428800

    while (tick == nil) do
        gapsTable[i] = iterator
        i = i + 1

        if pair == "GLP/ETH" then
            iterator = iterator - 4
        else
            iterator = iterator - 3600
        end

        if iterator < beginningOfTime then
            return nil
        end

        tick = box.space.currencypair_rate.index.currencyPairTimestampIndex:get({ pair, iterator })
    end

    for _, gap in pairs(gapsTable) do
        box.space.currencypair_rate:insert({pair, tick[2], gap, tick[4], tick[5]})
    end

    return box.space.currencypair_rate.index.currencyPairTimestampIndex:get({ pair, timestamp })
end

function stringsplit(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t = {}; i = 1
    for str in string.gmatch(inputstr, "([^" .. sep .. "]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end

currencyList = {}
fiatCurrencyList = {}

function initializeCurrencyData()

    if (#currencyList ~= 0 and #fiatCurrencyList ~= 0) then
        return
    end

    local i = 1
    local currency
    for _, t in box.space.currencypair_rate.index.timestampIndex:pairs() do
        currency = stringsplit(t[1], "/")[2]
        if (not inTable(currencyList, currency)) then
            table.insert(currencyList, currency)
        end
    end

    fiatCurrencyList = shallowCopy(currencyList)
    for i = 1, #fiatCurrencyList do
        if fiatCurrencyList[i] == 'ETH' then
            table.remove(fiatCurrencyList, i)
            break
        end
    end
end

function setupListOfCurrencies(pairs)
    fiatCurrencyList = shallowCopy(pairs)
    currencyList = shallowCopy(pairs)
    currencyList[#currencyList + 1] = 'ETH'
end

function getFiatCurrencyList()
    return fiatCurrencyList
end

function shallowCopy(t)
    local t2 = {}
    for k, v in pairs(t) do
        t2[k] = v
    end
    return t2
end

function inTable(tbl, item)
    for key, value in ipairs(tbl) do
        if value == item then return true end
    end
    return false
end

function currencyRatesByPairAndTimeRange(pair, from, to)
    return getTicks(pair, from, to, 'd')
end

function getLatestRates()

    local result = {}
    for i, currency in ipairs(currencyList) do
        result[i] = currentCurrencyRateByPair("GLP/" .. currency)
    end

    return result
end

function currencyRateByPairAndTimestamp(pair, timestamp)

    local result

    local beginningOfTime = 1526428800
    if timestamp < beginningOfTime then
        return "TIMESTAMP_TOO_OLD"
    end
    local timeNow = os.time(os.date("!*t"))
    if (timestamp > timeNow) then
        timestamp = timeNow
    end

    local roundedTimestamp
    if (pair == 'GLP/ETH') then
        roundedTimestamp = math.roundDown(timestamp, 4)
    else
        roundedTimestamp = math.roundDown(timestamp, 3600)
    end

    result = box.space.currencypair_rate.index.currencyPairTimestampIndex:get({ pair, roundedTimestamp })

    if result == nil then
        result = findAlternativeTick(pair, roundedTimestamp)
    end

    return result
end

function currentCurrencyRateByPair(pair)
    local timeNow = os.time(os.date("!*t"))
    return currencyRateByPairAndTimestamp(pair, timeNow)
end

function removeDataOlderThanTimestamp(timestamp)
    local counter = 0
    for _, t in box.space.currencypair_rate.index.timestampIndex:pairs({ timestamp }, { iterator = box.index.LT }) do
        box.space.currencypair_rate.index.hashIndex:delete(t[1])
        counter = counter + 1
    end
    return counter
end

function lastSuccessfulUpdate()
    local beginningOfTime = 1526428800

    local timeNow = os.time(os.date("!*t"))
    local secondsInHour = 3600

    local roundedToHour = math.round(timeNow, secondsInHour)
    if timeNow < roundedToHour then --if was rounded up - we want closest in past
        roundedToHour = roundedToHour - secondsInHour
    end

    local candidate = roundedToHour
    while candidate > beginningOfTime do
        local entry = box.space.currencypair_rate.index.currencyPairTimestampIndex:get({ 'ETH/ZAR', candidate })

        if entry then
            return entry[3]
        end

        candidate = candidate - secondsInHour
    end
    return 0
end

function math.sign(v)
    return (v >= 0 and 1) or -1
end

function math.round(v, bracket)
    bracket = bracket or 1
    return math.floor(v / bracket + math.sign(v) * 0.5) * bracket
end

function math.roundUp(v, bracket)
    bracket = bracket or 1
    return math.floor(v / bracket + math.sign(v) * 0.999999) * bracket
end

function math.roundDown(v, bracket)
    bracket = bracket or 1
    return math.floor(v / bracket + math.sign(v) * 0.000001) * bracket
end