package nosql

import (
	"github.com/pkg/errors"
	"github.com/tarantool/go-tarantool"
	"math/big"
)

/**
  Types
*/
type CurrencyRateEntity struct {
	CurrencyPair string
	Rate         *big.Float
	Timestamp    uint32
	TimestampReal uint32
	VendorId     string
}

type CurrencyRateDao struct{}

func NewCurrencyRateDao() *CurrencyRateDao {
	return &CurrencyRateDao{}
}

func (self *CurrencyRateDao) SetupListOfCurrencies(pairs []string) (*tarantool.Response, error) {
	_, err := client.Call("setupListOfCurrencies", []interface{}{pairs})
	if err != nil {
		return nil, err
	}
	return nil, nil
}

func (self *CurrencyRateDao) InsertCurrencyRate(currencyRate *CurrencyRateEntity) (*tarantool.Response, error) {
	return client.Insert(CURRENCY_RATE_SPACE_NAME, []interface{}{currencyRate.CurrencyPair, currencyRate.Rate.SetMode(big.ToNearestEven).Text('f', 8), currencyRate.Timestamp, currencyRate.TimestampReal, currencyRate.VendorId})
}

func (self *CurrencyRateDao) UpsertCurrencyRate(currencyRate *CurrencyRateEntity) (*tarantool.Response, error) {
	return client.Upsert(CURRENCY_RATE_SPACE_NAME, []interface{}{currencyRate.CurrencyPair, currencyRate.Rate.SetMode(big.ToNearestEven).Text('f', 8), currencyRate.Timestamp, currencyRate.TimestampReal, currencyRate.VendorId},
		[]interface{}{
			[]interface{}{"=", 1, currencyRate.Rate.SetMode(big.ToNearestEven).Text('f', 8)},
		})
}

func (self *CurrencyRateDao) GetCurrencyList() ([]string, error) {
	resp, err := client.Call("getFiatCurrencyList", []interface{}{})
	if err != nil {
		return nil, err
	}
	return self.unpackListToStrings(resp.Tuples())
}

func (self *CurrencyRateDao) LastSuccessfulUpdate() (int64, error) {
	resp, err := client.Call("lastSuccessfulUpdate", []interface{}{})
	if err != nil {
		return 0, err
	}
	timestamp := resp.Data[0].([]interface{})[0].(uint64)
	return int64(timestamp), nil
}

func (self *CurrencyRateDao) GetRateByPairAndTimestamp(pair string, timestamp int64) (*CurrencyRateEntity, error) {
	resp, err := client.Call("currencyRateByPairAndTimestamp", []interface{}{pair, timestamp})
	if err != nil {
		return nil, err
	}
	if resp == nil {
		return nil, errors.New("No Rate for " + pair + ", " + string(timestamp))
	}
	return self.unpack(resp.Tuples())
}

func (self *CurrencyRateDao) GetCurrencyRatesByPairAndTimeRange(pair string, from, to uint32) ([]*CurrencyRateEntity, error) {
	resp, err := client.Call("currencyRatesByPairAndTimeRange", []interface{}{pair, from, to})
	if err != nil {
		return nil, err
	}
	if resp == nil {
		return nil, errors.New("No Rates!")
	}
	return self.unpackList(resp.Tuples())
}

func (self *CurrencyRateDao) GetCurrentRateByPair(pair string) (*CurrencyRateEntity, error) {
	resp, err := client.Call("currentCurrencyRateByPair", []interface{}{pair})
	if err != nil {
		return nil, err
	}
	if resp == nil {
		return nil, errors.New("No Rate!")
	}
	return self.unpack(resp.Tuples())
}

func (self *CurrencyRateDao) GetLatestRates() ([]*CurrencyRateEntity, error) {
	resp, err := client.Call("getLatestRates", []interface{}{})
	if err != nil {
		return nil, err
	}
	if resp == nil {
		return nil, errors.New("NO_RATE")
	}
	return self.unpackList(resp.Tuples())
}

func (self *CurrencyRateDao) RemoveDataOlderThanTimestamp(timestamp int64) (uint64, error) {
	resp, err := client.Call("removeDataOlderThanTimestamp", []interface{}{timestamp})
	if err != nil {
		return 0, err
	}
	return resp.Data[0].([]interface{})[0].(uint64), nil
}

func (self *CurrencyRateDao) GetLatestRatesInMap(ratesMap map[string]*CurrencyRateEntity) error {
	rates, err := self.GetLatestRates()
	if err != nil {
		return err
	}

	if ratesMap == nil {
		ratesMap = make(map[string]*CurrencyRateEntity, len(rates))
	}

	for _, element := range rates {
		if element != nil {
			ratesMap[element.CurrencyPair] = element
		}
	}

	return nil
}

func (self *CurrencyRateDao) unpackListToStrings(respData [][]interface{}) ([]string, error) {
	if len(respData) == 0 {
		return nil, nil
	}
	respArr := make([]string, len(respData[0]))
	for i, data := range respData[0] {
		respArr[i] = data.(string)
	}
	if respArr[0] == "" {
		return nil, nil
	}
	return respArr, nil
}

func (self *CurrencyRateDao) unpack(respData [][]interface{}) (*CurrencyRateEntity, error) {
	entityList, err := self.unpackList(respData)
	if err != nil {
		return nil, err
	}
	if entityList == nil {
		return nil, nil
	}
	return entityList[0], nil
}

func (self *CurrencyRateDao) unpackList(respData [][]interface{}) ([]*CurrencyRateEntity, error) {
	if len(respData[0]) == 0 || respData[0][0] == nil {
		return nil, errors.New("NO_RATES")
	}
	var entity *CurrencyRateEntity = nil
	respArr := make([]*CurrencyRateEntity, len(respData))

	for i, data := range respData {
		if len(data) > 0 && data[0] != nil {
			if data[0].(string) == "TIMESTAMP_TOO_OLD" {
				return nil, nil
			}
			entity = &CurrencyRateEntity{}
			respArr[i] = entity
			entity.CurrencyPair = data[0].(string)
			rateInString := data[1].(string)
			rateInFloat, ok := new(big.Float).SetPrec(128).SetMode(big.ToNearestEven).SetString(rateInString)
			if !ok {
				return nil, errors.New("Could not format rate: " + rateInString)
			}
			entity.Rate = rateInFloat
			entity.Timestamp = uint32(data[2].(uint64))
			entity.TimestampReal = uint32(data[3].(uint64))
			entity.VendorId = data[4].(string)
		}
	}
	if respArr[0] == nil {
		return nil, nil
	}

	return respArr, nil
}
