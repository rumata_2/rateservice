package nosql

import (
	"testing"
	"reflect"
)

var currencyRateDao = NewCurrencyRateDao()

var expectedResponseWithEntities = make([]*CurrencyRateEntity, 2)
var expectedResponseWithPairs = []string{"ETH/AUD", "ETH/USD"}

var mockedResponse1 = [][]interface{}{
	{uint64(1), "ETH/AUD", "1.5123", uint64(1523546177), "mocked"},
	{uint64(21), "ETH/USD", "2.2343", uint64(1523546177), "mocked"},
}
var mockedResponse2 = [][]interface{}{
	[]interface{}{"ETH/AUD", "ETH/USD"},
}

func init() {
	//expectedResponseWithEntities[0] = &CurrencyRateEntity{1, "ETH/AUD", "1.5123", uint32(1523546177), "mocked"}
	//expectedResponseWithEntities[1] = &CurrencyRateEntity{21, "ETH/USD", "2.2343", uint32(1523546177), "mocked"}
}

func Test_ShouldUnpackCurrencyRateEntities(t *testing.T) {
	var list, err = currencyRateDao.unpackList(mockedResponse1)

	if err != nil {
		panic(err)
	}

	if !reflect.DeepEqual(expectedResponseWithEntities, list) {
		t.Errorf("Response was not as expected! %s", list)
	}
}

func Test_ShouldUnpackRateEntitiesToStrings(t *testing.T) {
	var list, err = currencyRateDao.unpackListToStrings(mockedResponse2)

	if err != nil {
		panic(err)
	}

	if !reflect.DeepEqual(expectedResponseWithPairs, list) {
		t.Errorf("FAIL!")
	}
}